﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayMeshDetailsScript : MonoBehaviour
{
    private Text m_text;

    // Start is called before the first frame update
    void Start()
    {
       m_text = GetComponent<Text>();
    }

    // Update the value of the given int field
    public void UpdateText(int n)
    {
        if (m_text)
        {
            m_text.text = n.ToString();
        }
    }

    // Update the value of the given float field
    public void UpdateText(float value)
    {
        if (m_text)
        {
            m_text.text = value.ToString();
        }
    }
}
