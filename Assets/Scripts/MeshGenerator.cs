﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// Events needed for publishing the various mesh details
[System.Serializable]
public class IntEvent : UnityEvent<int>
{
}

[System.Serializable]
public class FloatEvent : UnityEvent<float>
{
}

[RequireComponent(typeof(MeshFilter))]
public class MeshGenerator : MonoBehaviour
{
    // All the constants
    private static readonly float goldenRatio = (1.0f + Mathf.Sqrt(5.0f)) / 2.0f;
    // We consider only unit spheres. However this can be extended
    // to other radius spheres easily by changing this constant.
    private const float sphereRadius = 1.0f;

    // All the mesh data.
    private Vector3[] m_vertices;
    private Vector3[] m_normals;
    private int[] m_faceIdxs;

    // Dictionary for storing the index of the previously subdivided edge in order
    // to avoid the subdiving the same edge once again. 
    // Here we could have used a 2D array for faster caching perfomance 
    // but memory consumption is high.
    private Dictionary<KeyValuePair<int, int>, int> m_subDivEdgesCache = new Dictionary<KeyValuePair<int, int>, int>();
    private Mesh m_mesh;

    // Current subdivision level information.
    private int m_currNoIters = 0;
    private int m_subDivBase = 1;
  
    // Different events needed to display various mesh properties 
    // when the mesh changes.
    public IntEvent onFrequencyChange;
    public IntEvent onFacesChange;
    public IntEvent onEdgesChange;
    public IntEvent onVerticesChange;
    public IntEvent onIterationsChange;
    public FloatEvent onVolumeChange;
    public FloatEvent onErrorChange;

    private Vector3 ProjectOnSphere(float radius, Vector3 v)
    {
        return v.normalized * radius;
    }

    // Volume calculation is taken from 
    // https://en.wikipedia.org/wiki/Polyhedron
    public float GetSphereMeshVolume()
    {
        float totalVolume = 0.0f;
        Vector3 crossProd;
        for (int iTriIdx = 0; iTriIdx < m_faceIdxs.Length; iTriIdx += 3)
        {
            crossProd = Vector3.Cross(m_vertices[m_faceIdxs[iTriIdx + 1]] - m_vertices[m_faceIdxs[iTriIdx]], m_vertices[m_faceIdxs[iTriIdx + 2]] - m_vertices[m_faceIdxs[iTriIdx]]);
            totalVolume += crossProd.magnitude * Vector3.Dot(crossProd.normalized, m_vertices[m_faceIdxs[iTriIdx]]) / 2.0f;
        }

        return totalVolume / 3.0f;
    }

    // Percentage error in volume.
    public float GetSphereVolumeError()
    {
        float sphereVolume = 4.0f * 3.141592653589793238f * Mathf.Pow(sphereRadius, 3.0f) / 3.0f;
        return (sphereVolume - GetSphereMeshVolume()) / sphereVolume * 100;
    }

    // Subdivide the edge at the mid point.
    private int MidPoint(int pntIdx0, int pntIdx1, ref int highestIdx)
    {
        int smallerIdx = Math.Min(pntIdx0, pntIdx1);
        int greaterIdx = Math.Max(pntIdx0, pntIdx1);

        KeyValuePair<int, int> key = new KeyValuePair<int, int>(smallerIdx, greaterIdx);

        // If the edge is already divided then return the
        // index of the vertex at the subdivision.
        if(m_subDivEdgesCache.ContainsKey(key))
        {
            return m_subDivEdgesCache[key];
        }

        ++highestIdx;
        Vector3 newVertex = ProjectOnSphere(sphereRadius, (m_vertices[smallerIdx] + m_vertices[greaterIdx]) / 2);
        m_vertices[highestIdx] = newVertex;
        m_normals[highestIdx] = newVertex.normalized;

        m_subDivEdgesCache[key] = highestIdx;
        return highestIdx;

    }

    // Here we are using the class 1 subdivision
    // with subdivision description (2^iterations, 0)
    // as mentioned in https://en.wikipedia.org/wiki/Geodesic_polyhedron
    private void SubdivideFaces(int remainIters)
    {
        int frequency = m_subDivBase * m_subDivBase * 4;
        int nVertices = 10 * frequency + 2;
        Array.Resize(ref m_vertices, nVertices);
        Array.Resize(ref m_normals, nVertices);

        for (int iIter = 0; iIter < remainIters; ++iIter)
        {
            m_subDivEdgesCache.Clear();
            int highestIndex = 10 * m_subDivBase * m_subDivBase + 1;
            // The previous index of the previous vertices array is
            // needed as we have increased the vertices size we need
            // to keep track of where to add the new vertices from in
            // the resized array.
            m_subDivBase = 2 * m_subDivBase;
            int nTriIds = m_faceIdxs.Length;

            // Assing storage for stroring the new triangle vertex indices.
            int[] trainglesSubdiv = new int[60 * m_subDivBase * m_subDivBase];
            int iNewTriIdx = 0;
            for (int iTriIdx = 0; iTriIdx < nTriIds; iTriIdx += 3)
            {
                // Subdivide each edge of the triangle and form 
                // four triangles from one triangle based.
                int v1 = MidPoint(m_faceIdxs[iTriIdx], m_faceIdxs[iTriIdx + 1], ref highestIndex);
                int v2 = MidPoint(m_faceIdxs[iTriIdx + 1], m_faceIdxs[iTriIdx + 2], ref highestIndex);
                int v3 = MidPoint(m_faceIdxs[iTriIdx + 2], m_faceIdxs[iTriIdx], ref highestIndex);
                trainglesSubdiv[iNewTriIdx++] = m_faceIdxs[iTriIdx]; trainglesSubdiv[iNewTriIdx++] = v1; trainglesSubdiv[iNewTriIdx++] = v3;
                trainglesSubdiv[iNewTriIdx++] = m_faceIdxs[iTriIdx + 1]; trainglesSubdiv[iNewTriIdx++] = v2; trainglesSubdiv[iNewTriIdx++] = v1;
                trainglesSubdiv[iNewTriIdx++] = m_faceIdxs[iTriIdx + 2]; trainglesSubdiv[iNewTriIdx++] = v3; trainglesSubdiv[iNewTriIdx++] = v2;
                trainglesSubdiv[iNewTriIdx++] = v1; trainglesSubdiv[iNewTriIdx++] = v2; trainglesSubdiv[iNewTriIdx++] = v3;
            }

            m_faceIdxs = trainglesSubdiv;
        }
    }

    // Revert back sub divided faces by merging them
    private void MergeFaces(int remainIters)
    {
        for (int iIter = 0; iIter < remainIters; ++iIter)
        {
            m_subDivBase /= 2;
        }
        int frequency = m_subDivBase * m_subDivBase;
        int nVertices = 10 * frequency + 2;
        Array.Resize(ref m_vertices, nVertices);
        Array.Resize(ref m_normals, nVertices);

        int nTriIds = m_faceIdxs.Length;
        int[] trainglesSubdiv = new int[60 * frequency];
        int iNewTriIdx = 0;

        // Just remove the vertices and there indices from face indices
        // array from the highee subdivision level.
        for (int iTriIdx = 0; iTriIdx < nTriIds; iTriIdx += 12 * remainIters)
        {
            trainglesSubdiv[iNewTriIdx++] = m_faceIdxs[iTriIdx];
            trainglesSubdiv[iNewTriIdx++] = m_faceIdxs[iTriIdx + 3 * remainIters];
            trainglesSubdiv[iNewTriIdx++] = m_faceIdxs[iTriIdx + 6 * remainIters];
        }

        m_faceIdxs = trainglesSubdiv;
    }

    private void CopyMeshDataToMeshFilter()
    {
        if (m_mesh == null)
        {
            m_mesh = new Mesh();
        }
        else
        {
            m_mesh.Clear();
        }
        GetComponent<MeshFilter>().mesh = m_mesh;
        m_mesh.vertices = m_vertices;
        m_mesh.normals = m_normals;
        m_mesh.triangles = m_faceIdxs;
    }

    // Call this method when the mesh changes
    // and hence the propeties changes have to 
    // be notified.
    private void CallMeshDetailsChange()
    {
        int frequency = m_subDivBase * m_subDivBase;
        onFrequencyChange.Invoke(frequency);
        onFacesChange.Invoke(m_faceIdxs.Length / 3);
        onEdgesChange.Invoke(30 * frequency);
        onVerticesChange.Invoke(10 * frequency + 2);
        onIterationsChange.Invoke(m_currNoIters);
        onVolumeChange.Invoke(GetSphereMeshVolume());
        onErrorChange.Invoke(GetSphereVolumeError());
    }

    // Start is called before the first frame update
    void Start()
    {
        // Initialize the Geodesic Icosahedron base form as
        // this has the best isoperimetric quotient 
        // among the platonic solids mentioned.
        m_vertices = new Vector3[12];
        m_vertices[0] = ProjectOnSphere(1.0f, new Vector3(-1.0f, goldenRatio, 0.0f));
        m_vertices[1] = ProjectOnSphere(1.0f, new Vector3(1.0f, goldenRatio, 0.0f));
        m_vertices[2] = ProjectOnSphere(1.0f, new Vector3(-1.0f, -goldenRatio, 0.0f));
        m_vertices[3] = ProjectOnSphere(1.0f, new Vector3(1.0f, -goldenRatio, 0.0f));

        m_vertices[4] = ProjectOnSphere(1.0f, new Vector3(0.0f, -1.0f, goldenRatio));
        m_vertices[5] = ProjectOnSphere(1.0f, new Vector3(0.0f, 1.0f, goldenRatio));
        m_vertices[6] = ProjectOnSphere(1.0f, new Vector3(0.0f, -1.0f, -goldenRatio));
        m_vertices[7] = ProjectOnSphere(1.0f, new Vector3(0.0f, 1.0f, -goldenRatio));

        m_vertices[8] = ProjectOnSphere(1.0f, new Vector3(goldenRatio, 0, -1.0f));
        m_vertices[9] = ProjectOnSphere(1.0f, new Vector3(goldenRatio, 0, 1.0f));
        m_vertices[10] = ProjectOnSphere(1.0f, new Vector3(-goldenRatio, 0, -1.0f));
        m_vertices[11] = ProjectOnSphere(1.0f, new Vector3(-goldenRatio, 0, 1.0f));

        m_normals = new Vector3[12];
        for (int i = 0; i < 12; ++i)
        {
           m_normals[i] = m_vertices[i].normalized;
        }

        m_faceIdxs = new int[60];
        m_faceIdxs[0] = 0; m_faceIdxs[1] = 11; m_faceIdxs[2] = 5;
        m_faceIdxs[3] = 0; m_faceIdxs[4] = 5; m_faceIdxs[5] = 1;
        m_faceIdxs[6] = 0; m_faceIdxs[7] = 1;m_faceIdxs[8] = 7;
        m_faceIdxs[9] = 0;m_faceIdxs[10] = 7;m_faceIdxs[11] = 10;
        m_faceIdxs[12] = 0;m_faceIdxs[13] = 10;m_faceIdxs[14] = 11;

        m_faceIdxs[15] = 1;m_faceIdxs[16] = 5;m_faceIdxs[17] = 9;
        m_faceIdxs[18] = 5;m_faceIdxs[19] = 11;m_faceIdxs[20] = 4;
        m_faceIdxs[21] = 11;m_faceIdxs[22] = 10;m_faceIdxs[23] = 2;
        m_faceIdxs[24] = 10;m_faceIdxs[25] = 7;m_faceIdxs[26] = 6;
        m_faceIdxs[27] = 7;m_faceIdxs[28] = 1;m_faceIdxs[29] = 8;

        m_faceIdxs[30] = 3;m_faceIdxs[31] = 9;m_faceIdxs[32] = 4;
        m_faceIdxs[33] = 3;m_faceIdxs[34] = 4;m_faceIdxs[35] = 2;
        m_faceIdxs[36] = 3;m_faceIdxs[37] = 2;m_faceIdxs[38] = 6;
        m_faceIdxs[39] = 3;m_faceIdxs[40] = 6;m_faceIdxs[41] = 8;
        m_faceIdxs[42] = 3;m_faceIdxs[43] = 8;m_faceIdxs[44] = 9;

        m_faceIdxs[45] = 4;m_faceIdxs[46] = 9;m_faceIdxs[47] = 5;
        m_faceIdxs[48] = 2;m_faceIdxs[49] = 4;m_faceIdxs[50] = 11;
        m_faceIdxs[51] = 6;m_faceIdxs[52] = 2;m_faceIdxs[53] = 10;
        m_faceIdxs[54] = 8;m_faceIdxs[55] = 6;m_faceIdxs[56] = 7;
        m_faceIdxs[57] = 9;m_faceIdxs[58] = 8;m_faceIdxs[59] = 1;

        CallMeshDetailsChange();

        CopyMeshDataToMeshFilter();
    }

    // Update the mesh when the iteration count is changed.
    public void UpdateMesh(float iters)
    {
        int iterations = Mathf.RoundToInt(iters);
        int remainIters = iterations - m_currNoIters;
        m_currNoIters = iterations;

        // Subdivide when the iteration level increases
        // else merge the previous subdivide mesh.
        if (remainIters > 0)
        {
            SubdivideFaces(remainIters);
        }
        else
        {
            remainIters = -remainIters;
            MergeFaces(remainIters);
        }

        CallMeshDetailsChange();

        CopyMeshDataToMeshFilter();
    }
}
